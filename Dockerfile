FROM kuhlmannmarkus/qresdiot
RUN apt install nginx -y
COPY index.html /var/www/html/index.html
COPY start.sh /start.sh
COPY loading.gif /var/www/html/loading.gif
EXPOSE 80
EXPOSE 1234
EXPOSE 1235
CMD ["./start.sh"]